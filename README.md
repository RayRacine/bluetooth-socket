bluetooth-socket
================
Racket impl of FFI RAW, AF_BLUETOOTH BTPROTO_HCI socket for Linux.
Allows for communicating to a Bluetooth Controller (hciX) via HCI
packets per the Bluetooth Specification.

Currently only supports Linux.  Support for Mac patch appreciated.