#lang info
(define collection "bluetooth-socket")
(define deps '("base"))
(define build-deps '("scribble-lib" "racket-doc" "rackunit-lib"))
(define scribblings '(("scribblings/bluetooth-socket.scrbl" ())))
(define pkg-desc "Allows for communicating to a Bluetooth Controller (hciX) via HCI
packets per the Bluetooth Specification.")
(define version "0.1")
(define pkg-authors '("Raymond Racine"))

