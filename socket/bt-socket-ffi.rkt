#|
    Bluetooth Socket Collection.
    Copyright (C) 2018 Raymond Racine ray.racine@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
|#

#lang racket/base

(require ffi/unsafe
         ffi/unsafe/define)
(provide (protect-out (all-defined-out)))

(define platform
  (case (system-type 'os)
    [(maxosx) 'bsd]
    [(unix)
     (define machine
       ;; security guard may prevent executing uname
       (with-handlers ([exn:fail? (λ (e) "unknown")])
         (system-type 'machine)))
     (cond [(regexp-match? #rx"^Linux" machine) 'linux]
           [(regexp-match? #rx"^[a-zA-Z]*BSD" machine) 'bsd]
           [else #f])]
    [else #f]))

(define bt-socket-available? #t)


;; Constants

(define HCI_MAX_DEV 16)

;; Linux Constants

;; linux: sys/socket.h; bsd/macosx: sys/socket.h
(define SHUT_RD 0)
(define SHUT_WR 1)

;; linux: asm-generic/{errno-base,errno}.h; bsd/macosx: sys/errno.h
(define EINTR           4)
(define EAGAIN          (case platform [(linux) 11]  [(bsd) 35]))
(define EWOULDBLOCK     EAGAIN)
(define EINPROGRESS     (case platform [(linux) 115] [(bsd) 36]))
(define ENOTCONN        (case platform [(linux) 107] [(bsd) 57]))

(define SOCK_RAW 3)

(define AF_BLUETOOTH 31)

(define BTPROTO_L2CAP  0)
(define BTPROTO_HCI    1) ;; Currently only HCI is supported
(define BTPROTO_SCO    2)
(define BTPROTO_RFCOMM 3)

(define HCI_CHANNEL_RAW     0)
(define HCI_CHANNEL_USER    1)
(define HCI_CHANNEL_MONITOR 2)
(define HCI_CHANNEL_CONTROL 3)
(define HCI_CHANNEL_LOGGING 4)

;; setsockopt level and optname
;; man setsockopt
(define SOL_HCI    0)
(define HCI_FILTER 2)

(define UNIX-PATH-MAX (case platform [(linux) 108] [else 104]))

(define _sa_family_t _ushort) ;; bits/sockaddr.h

; #include <sys/ioctl.h>
; #include <stdio.h>
; 
; int main (void) {
;   printf("%u\n", _IOR ('H', 210, int)); # HCI_LIST_DEV
;   printf("%u\n", _IOR ('H', 211, int)); # HCI_GET_INFO
; }


;; ioctl values to get the dev list and the info for a specific device
(define HCI-GET-DEV-LIST 2147764434) ; _IOR ('H', 210, int)
(define HCI-GET-DEV-INFO 2147764435) ; _IOR ('H', 211, int)

;; C-Structures

(define _sa_family (case platform [(linux) _ushort] [else _ubyte]))

(define-cstruct _sockaddr_hci
  ([hci_family  _sa_family_t]
   [hci_dev     _ushort]
   [hci_channel _ushort]))

(define-cstruct _hci_dev_req
  ([dev_num _uint16]
   [dev_opt _uint32]))

(define-cstruct _hci_dev_list_req
  ([dev_num _uint16]
   [dev_req (make-array-type _hci_dev_req HCI_MAX_DEV)]))

(define-cstruct _hci_dev_info
  ([dev_id _uint16]
   [name (make-array-type _byte 8)]

   [bdaddr (make-array-type _uint8 6)]

   [flags _uint32]
   [type _uint8]

   [features (make-array-type _uint8 8)]
   
   [pkt_type    _uint32]
   [link_policy _uint32]
   [link_mode   _uint32]

   [acl_mtu  _uint16]
   [acl_pkts _uint16]
   [sco_mtu  _uint16]
   [sco_pkts _uint16]

   ;; hci_dev stats
   [err_rx _uint32]
   [err_tx _uint32]
   [cmd_tx _uint32]
   [evt_rx _uint32]
   [acl_tx _uint32]
   [acl_rx _uint32]
   [sco_tx _uint32]
   [sco_rx _uint32]
   [byte_rx _uint32]
   [byte_tx _uint32]))


(define-cstruct _hci_filter
  ([type_mask    _uint32]
   [event_mask_1 _uint32]
   [event_mask_2 _uint32]
   [opcode       _uint16]))

;; System Functions

(define (check v who)
  (unless (zero? v)
    (error who "failed: ~a" v)))

(define (check-ok v who)
  (unless (> v -1)
    (check v who))
  v)

(define-ffi-definer define-libc (ffi-lib #f)
  #:default-make-fail make-not-available)

(define-libc socket
  (_fun #:save-errno 'posix
        _int _int _int -> (r : _int) -> (check-ok r 'bt-socket)))

;(define-libc connect
;  (_fun #:save-errno 'posix
;        _int _sockaddr_un-pointer _int -> _int))
;
(define-libc bind
  (_fun #:save-errno 'posix
        _int _sockaddr_hci-pointer _int -> (r : _int) -> (check r 'bind)))

(define-libc close
  (_fun #:save-errno 'posix
        _int -> _int))

;; man 3 shutdown
(define-libc shutdown
  (_fun #:save-errno 'posix
        _int _int -> (r : _int) -> (check r 'shutdown)))

;(define-libc getsockopt
;  (_fun #:save-errno 'posix
;        _int _int _int (value : (_ptr io _int) = 0) (len : (_ptr io _uint32) = (ctype-sizeof _int))
;        -> (result : _int)
;        -> (cond [(zero? result) value]
;                 [else (error 'getsockopt "error ~a" (errno-error-lines (saved-errno)))])))

(define-libc setsockopt
  (_fun #:save-errno 'posix
        _int _int _int _hci_filter-pointer _uint32 
        -> (r : _int) -> (check r 'setsockopt)))

        ;;-> (cond [(zero? result) value]
        ;;         [else (error 'setsockopt "error ~a" (errno-error-lines (saved-errno)))])))

(define-libc ioctl
  (_fun #:save-errno 'posix
        _int _ulong _pointer -> (r : _int) -> (check r 'ioctl)))

(define-libc scheme_make_fd_output_port
  (_fun _int _racket _bool _bool _bool -> _racket))

(define strerror-name
  (case platform
    [(linux) "__xpg_strerror_r"]
    [else    "strerror_r"]))

(define strerror_r
  (get-ffi-obj strerror-name #f
               (_fun (errno) ::
                     (errno : _int)
                     (buf : _bytes = (make-bytes 1000))
                     (buf-len : _size = (bytes-length buf))
                     -> _void
                     -> (cast buf _bytes _string/locale))
               (lambda ()
                 (lambda (errno) #f))))

(define (errno-error-lines errno)
  (define err (strerror_r errno))
  (format "\n  errno: ~a~a" errno (if err (format "\n  error: ~a" err) "")))
