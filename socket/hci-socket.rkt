#|
    Bluetooth Socket Collection.
    Copyright (C) 2018 Raymond Racine ray.racine@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
|#

#lang racket/base

(provide hci-connect
         hci-read-semaphore
         hci-disconnect
         hci-initialize-socket
         hci-socket
         hci-socket-close
         hci-devices
         hci-device-info)

(require ffi/unsafe
         (only-in ffi/unsafe/port unsafe-file-descriptor->semaphore unsafe-file-descriptor->port)
         (only-in ffi/unsafe/atomic call-as-atomic)
         (only-in ffi/unsafe/custodian register-custodian-shutdown unregister-custodian-shutdown)
         "bt-socket-ffi.rkt"
         "stats.rkt")

;; UART HCI Packet types
(define HCI-COMMAND-PKT             #x01)
(define HCI-EVENT-PKT               #x04)
(define HCI-ACLDATA-PKT             #x02)

;; Event types
(define EVT-DISCONN-COMPLETE        #x05)
(define EVT-ENCRYPT-CHANGE          #x08)
(define EVT-CMD-COMPLETE            #x0e)
(define EVT-CMD-STATUS              #x0f)
(define EVT-NUMBER-COMPLETED-PACKETS-EVENT #x13)
(define EVT-LE-META-EVENT           #x3e)

;; Racket Constants for FD Event ops.
(define MZFD_CREATE_READ  1)
(define MZFD_CREATE_WRITE 2)
(define MZFD_REMOVE       5)

(define-syntax hci-mask
  (syntax-rules ()
    ((_ bit-num ...)
     (bitwise-ior (arithmetic-shift 1 bit-num) ...))))

(define (hci-socket)
  (socket AF_BLUETOOTH SOCK_RAW BTPROTO_HCI))

(define (hci-socket-close fid)
  (close fid))

(define (hci-raw-bind fd [dev-id 0])
  (define sock-addr-ptr (make-sockaddr_hci AF_BLUETOOTH dev-id HCI_CHANNEL_RAW))
  (bind fd sock-addr-ptr (ctype-sizeof _sockaddr_hci)))

(define socket-filter-settings-ptr
  (let ((type-mask  (hci-mask HCI-COMMAND-PKT HCI-EVENT-PKT HCI-ACLDATA-PKT))
        (evt-mask-1 (hci-mask EVT-DISCONN-COMPLETE EVT-ENCRYPT-CHANGE EVT-CMD-COMPLETE EVT-CMD-STATUS EVT-NUMBER-COMPLETED-PACKETS-EVENT))
        (evt-mask-2 (arithmetic-shift 1 (- EVT-LE-META-EVENT 32)))
        (opcode 0))
  (make-hci_filter type-mask evt-mask-1 evt-mask-2 opcode)))
  
(define (hci-device-info fd dev-id)
  (define feature-sz 8)
  (define bdaddr-sz 6)
  (define size (ctype-sizeof _hci_dev_info))
  (define dev-info-ptr (malloc size))
      
  (memset dev-info-ptr 0 size)
  (define info (ptr-ref dev-info-ptr _hci_dev_info))
  (set-hci_dev_info-dev_id! info dev-id)
                            
  (ioctl fd HCI-GET-DEV-INFO dev-info-ptr)

  (define name (let ((name-ptr (hci_dev_info-name info)))
                 (cast name-ptr _pointer _string)))

  (define features (let ((tgt (make-bytes feature-sz 0))
                         (src (ptr-ref (hci_dev_info-features info) (_array _uint8 feature-sz))))                     
                     (do ([i 0 (add1 i)])
                       ((>= i feature-sz) tgt)                       
                       (bytes-set! tgt i (array-ref src i)))))

  (define bdaddr (let ((bs (make-bytes bdaddr-sz 0))
                       (src (ptr-ref (hci_dev_info-bdaddr info) (_array _uint8 bdaddr-sz))))
                   (do ([i 0 (add1 i)])
                     ((>= i bdaddr-sz) bs)                    
                     (bytes-set! bs i (array-ref src i)))))
                                            
  (define stats (HciDeviceStats  (hci_dev_info-err_rx info)
                                 (hci_dev_info-err_tx info)
                                 (hci_dev_info-cmd_tx info)
                                 (hci_dev_info-evt_rx info)
                                 (hci_dev_info-acl_tx info)
                                 (hci_dev_info-acl_rx info)
                                 (hci_dev_info-sco_tx info)
                                 (hci_dev_info-sco_rx info)
                                 (hci_dev_info-byte_rx info)
                                 (hci_dev_info-byte_tx info)))

  (HciDeviceInfo (hci_dev_info-dev_id      info)
                 name
                 bdaddr
                 (hci_dev_info-flags       info)
                 (hci_dev_info-type        info)
                 features
                 (hci_dev_info-pkt_type    info)
                 (hci_dev_info-link_policy info)
                 (hci_dev_info-link_mode   info)
                 (hci_dev_info-acl_mtu     info)
                 (hci_dev_info-acl_pkts    info)
                 (hci_dev_info-sco_mtu     info)
                 (hci_dev_info-sco_pkts    info)
                 stats))

;; (: hci-devices (-> Socket (Listof Integer)))  i.e (Listof Dev-Ids) for found Bluetooth controllers. e.g. (0 1) => hci0 hci1 were found.
(define (hci-devices fd)
  (define req-sz (ctype-sizeof _hci_dev_list_req))
  (define req-buff-ptr (malloc req-sz))

  (memset req-buff-ptr 0 req-sz)
  (ptr-set! req-buff-ptr _uint16 HCI_MAX_DEV)

  (ioctl fd HCI-GET-DEV-LIST req-buff-ptr)
  
  (define req-list-ref (ptr-ref req-buff-ptr _hci_dev_list_req))

  (define dev-cnt  (hci_dev_list_req-dev_num req-list-ref))
  (define dev-array (ptr-ref (hci_dev_list_req-dev_req req-list-ref) (_array _hci_dev_req dev-cnt)))
  
  (let loop ((i 0) (dev-ids '()))
    (if (< i dev-cnt)
        (let ((dev-req (array-ref dev-array i)))
          (loop (add1 i) (cons (hci_dev_req-dev_num (ptr-ref dev-req _hci_dev_req)) dev-ids)))
        dev-ids)))

;; FIXME RPR - Pass in the dev-id to check and then parse out the flags to the is-up flag.
(define (hci-up? fd)
  (define size (ctype-sizeof _hci_dev_info))
  (define dev-info-ptr (malloc size))
      
  (memset dev-info-ptr 0 size)

  (ioctl fd HCI-GET-DEV-INFO dev-info-ptr)
  (displayln (hci_dev_info-type (ptr-ref dev-info-ptr _hci_dev_info)))
  (hci_dev_info-flags (ptr-ref dev-info-ptr _hci_dev_info)))

(define (set-socket-filter* fd type-mask event-mask-1 event-mask-2 opcode)
  (let ((filter-ptr (make-hci_filter type-mask event-mask-1 event-mask-2 opcode)))
    (setsockopt fd SOL_HCI HCI_FILTER filter-ptr (ctype-sizeof _hci_filter))))

(define (set-socket-filter fd)
  (setsockopt fd SOL_HCI HCI_FILTER socket-filter-settings-ptr (ctype-sizeof _hci_filter)))

;; raw-connect : Integer -> ValuesOf(Int, Registration)
(define (raw-connect [dev-id 0])
  (let* ((fd (hci-socket))
         (reg (register-custodian-shutdown fd close-unregister)))       
    (hci-raw-bind fd dev-id)
    (values fd reg)))

(define (hci-read-semaphore fd)
  (unsafe-file-descriptor->semaphore fd 'read))

;; close-unregister : FD -> Custodian -> Int
(define (close-unregister fd cust)
  (begin0
    (close fd)
    ;; (scheme_fd_to_semaphore fd MZFD_REMOVE #t) ;;remove any and all polling ready semaphores created for the fd
    (unregister-custodian-shutdown fd cust)))     ;; FIXME AUDIT  RPR - A lot of manual stuff is now auto-handled in the new Racket IO routines. 
    
;; make-socket-ports : Symbol FD Cust-Reg/#f -> (values Input-Port Output-Port)
(define (make-socket-ports socket-fd reg)
  (with-handlers ([(λ (e) #t)
                   (λ (exn)                    
                     (close-unregister socket-fd reg)                     
                     (raise exn))])
    (define-values (in out) (unsafe-file-descriptor->port socket-fd (string->bytes/utf-8 "HCI-PORT") '(read write))) ;; #t to create a read (in) port as well 
    ;; closing the ports closes socket-fd, so custodian no longer needs to manage the socket fd directly    
    (unregister-custodian-shutdown socket-fd reg) 
    (values in out)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (hci-connect mode dev-id)
  (case mode
    ([RAW] (let*-values (([fd reg] (raw-connect dev-id))
                         ([in out] (make-socket-ports fd reg)))
             (values fd in out)))
    (else (error 'HCI-MODE-CURRENTLY-NOT-SUPPORTED))))

(define (hci-disconnect fd inp outp)  
  (close-input-port inp)
  (close-output-port outp))

;; Initialize the socket as responsive to HCI events.
;; Otherwise will hang on any read attempt to the socket.
(define (hci-initialize-socket fd)
  (set-socket-filter fd))
